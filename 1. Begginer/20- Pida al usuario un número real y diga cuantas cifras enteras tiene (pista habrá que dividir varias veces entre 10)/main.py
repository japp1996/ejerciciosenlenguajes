def validNumber(num):
	# replace function (valueToFind, valueToReplace, LimitOfReplacement)
	floating = num.replace(".", "", 1) # 2.10 => True 20.1.2 => False
	if not floating.isdigit():
		print("Error: input a valid number.")
		exit()
	return num

# 1 with a string input
real = input("Input a real number: ")
real = validNumber(real)
count = 0
for string in real:
	count += 1
	if string == ".":
		print("El número " + str(real) + " tiene " + str(count) + " cifras enteras")
		break

# 2 with a float input

real = input("Input a real number: ")
real = int(validNumber(real))
reduced_num = real
count = 0
while True:
	reduced_num = reduced_num/10
	count += 1
	if (reduced_num < 1):
		print("El número " + str(real) + " tiene " + str(count) + " cifras enteras")
		break
