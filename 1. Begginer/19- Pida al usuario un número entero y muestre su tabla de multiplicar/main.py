def validNumber(num):
	if not num.isdigit():
		print("Ingrese un número válido")
		exit()
	return int(num)

num = input("Ingrese un número entero: ")
num = validNumber(num)
for i in range(1, 11):
	print(str(num) + " x " + str(i) + "= " + str(num*i))