"""
   Pida al usuario un número entero y muestre en pantalla sus divisores
   (exceptuando el 1 y el propio número).
   Por ejemplo, si se introduce 20, debería aparecer
	   	2 es divisor de 20
		4 es divisor de 20
		5 es divisor de 20
		10 es divisor de 20
"""

def validNumber(num):
	if not num.isdigit():
		print("Error: debe ingresar números.")
		exit()
	num = int(num)
	if num == 0 :
		print("El número 0 no es divisible entre ningún número")
		exit()

	return num

num = input("Ingrese un número entero")
num = validNumber(num)
count = 0
while count < num:
	count += 1
	res = num / count
	if not res.is_integer() or res == 1 or res == num:
		continue
	else:
		print(str(count) + " es divisor de " + str(num))