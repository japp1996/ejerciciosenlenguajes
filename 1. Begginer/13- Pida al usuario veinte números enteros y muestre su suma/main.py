
print("Welcome to the program to sum 20 numbers")
total = 0
count = 1
while count < 21 :
	entered = input("Input the number " + str(count) + ": ")
	if entered.isdigit():
		count += 1
		total += int(entered)
	else :
		print("Must be entered a number to continue")

print("The final result was " + str(total))