# Improve the 16th exercise to notice if the number has any divisor 

def validNumber(num):
	if not num.isdigit():
		print("Error: you must input an integer number")
		exit()
	num = int(num)
	if num == 0 :
		print("Number 0 is not divisible between any number")
		exit()

	return num


def FloatOneOrSame(num, count, res):
	# if is float or 1 or the same num then can divide
	return not res.is_integer() or res == 1 or res == num
	
num = input("Input an integer number: ")
num = validNumber(num)
count = 0
divcount = 0
while count < num:
	count += 1
	res = num / count
	if not FloatOneOrSame(num, count, res):
		print(str(count) + " is divisor of " + str(num))
		divcount += 1

	if count == num-1 and not divcount:
		print("Does not have divisors more than 1 and itself")
